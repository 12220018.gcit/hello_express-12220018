//load the express module
const express = require("express");
// create an oinstance of express application
const app = express();
const mustacheExpress = require("mustache-express");

const path = require("path");
const VIEWS_PATH = path.join(__dirname, "/views");

const session = require('express-session')

const studentRoutes = require("./routes/students");
const PORT = 3001;

//set app engin, mustache files extension
app.engine("mustache", mustacheExpress(VIEWS_PATH + "/partials", ".mustache"));

//set the location of the view files
app.set("views", VIEWS_PATH);
app.set("view engine", "mustache");

//Adding middleware 
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// register routes
app.use("/students", studentRoutes);
app.use(express.static("static"));



//starting server
app.listen(3001, function () {
  console.log("Server is running on PORT " + PORT);
});


const myLogger = function (req, res, next) {
console.log('LOGGED')
next()
}
app.use(myLogger)

 app.get('/', (req, res) => {
 res.send("ROOT")
 
 })

 app.get('/login', (req, res) => {
  res.send("LOGIN")
  })

  app.use (session({
    secret: 'sam-sam',
    resave: false,
    saveUninitialized: true,
  }))