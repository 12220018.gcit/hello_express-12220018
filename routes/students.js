//load the express module
const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  let user = {
    name: "Kuenden",
    programme: "IDD",
    year: "2nd",
    address: {
      Dzongkhag: "Paro",
      Country: "Bhutan",
    },
  };
  res.render("index", user);
});

router.get("/add-students", (req, res) => {
  res.render("stdForm");
});

//form post request
router.post('/add-student', (req, res) => {
  let name = req.body.name
  let program = req.body.program

  if (req.session) {
    req.session.name = name
    req.session.program = program
  }
  console.log(name)

  //index page
  router.get('/',authenticate,(req, res) => {
    let user = {
      name : req.session.name, programme : req.session.program,
      address: {
        Dzongkhag: "Paro",
        Country: "Bhutan"
      }
    }
    res.render('index', user)
  })
})

//creating middleware

function authenticate(req,res,next){
  //checking if session is initialized
  if(req.session){
      //check if user has provided name
        if(req.session.name){
          //carry on with the requst
          next()
        }

        else{
          //no provided name
          res.redirect('/students/add-students')
        }
  }

  else{
    //no session
    res.redirect('/students/add-students')
  }
}



//not sure sire
router.post("/add-students", (req, res) => {
    let name = req.body.name;
    let program = req.body.program;
    console.log(name);
    console.log(program);
  
    res.status(200).send();
  });
  module.exports = router;